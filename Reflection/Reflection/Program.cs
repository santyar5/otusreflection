﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Newtonsoft.Json;
using static System.Console;
using System.Runtime.Serialization.Formatters.Binary;

namespace Reflection
{
    class Foo
    {
        [JsonProperty("i1")]
        public int i1;
        [JsonProperty("i2")]
        public int i2;
        [JsonProperty("i3")]
        public int i3;
        [JsonProperty("i4")]
        public int i4;
        [JsonProperty("i5")]
        public int i5;
        public Foo Get() => new Foo() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
        public Foo() { }
        public Foo(int i1, int i2, int i3, int i4, int i5)
        {
            this.i1 = i1;
            this.i2 = i2;
            this.i3 = i3;
            this.i4 = i4;
            this.i5 = i5;
        }
    }
    class Program
    {
        public static Foo mainFoo = new Foo().Get();
        public static List<Foo> foos = new List<Foo>();
        static void Main(string[] args)
        {
            Stopwatch t = new Stopwatch();

            t.Start();
            CreateData();
            WriteLine($"10.000 objects \"Foo\" created in {t.ElapsedMilliseconds} ms.");

            t.Restart();
            SerializeToCSV();
            WriteLine($"10.000 objects \"Foo\" serialized to C:\\temp\\Foo.csv in {t.ElapsedMilliseconds} ms.");

            t.Restart();
            int csvObjCount = DeserializeFromCSV();
            WriteLine($"Deserialized {csvObjCount} objects from C:\\temp\\Foo.csv in {t.ElapsedMilliseconds} ms.");

            t.Restart();
            SerializeToJSON();
            WriteLine($"10.000 objects \"Foo\" serialized to C:\\temp\\Foo.json in {t.ElapsedMilliseconds} ms.");

            t.Restart();
            int jsonObjCount = DeserializeFromJSON();
            WriteLine($"Deserialized {jsonObjCount} objects from JSON file in {t.ElapsedMilliseconds} ms.");
            ReadLine();

        }

        static void CreateData()
        {
            for (int i = 0; i < 10_000; i++)
            {
                foos.Add(mainFoo);
            }
        }

        static void SerializeToCSV()
        {
            Type type = typeof(Foo);
            FieldInfo[] fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
            var sb = new StringBuilder();

            foreach (Foo foo in foos)
            {
                foreach (FieldInfo fld in fields)
                {
                    sb.Append(fld.GetValue(foo)).Append(';');
                }
                sb.Length--; // Remove last ";"
                sb.AppendLine();
            }

            File.AppendAllText(@"C:\temp\Foo.csv", sb.ToString());
        }

        static int DeserializeFromCSV()
        {
            List<Foo> foos = new List<Foo>();
            string obj = File.ReadAllText(@"C:\temp\Foo.csv");
            Type type = typeof(Foo);
            FieldInfo[] fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);
            foreach (string wholeString in obj.Split('\n'))
            {
                if (!string.IsNullOrWhiteSpace(wholeString))
                {
                    Foo foo = new Foo();
                    string[] values = wholeString.Split(';');

                    for (int i = 0; i < fields.Length; i++)
                    {
                        //не придумал, как приводить к типу, в зависимости от того, что в Foo
                        fields[i].SetValue(foo, int.Parse(values[i]));
                    }
                    foos.Add(foo);
                }
            }
            return foos.Count;
        }

        static void SerializeToJSON()
        {
            var sb = new StringBuilder();
            foreach (Foo foo in foos)
            {
                string json = JsonConvert.SerializeObject(foo);
                sb.Append(json);
                sb.AppendLine();
            }

            File.AppendAllText(@"C:\temp\Foo.json", sb.ToString());
        }

        static int DeserializeFromJSON()
        {
            string obj = File.ReadAllText(@"C:\temp\Foo.json");
            List<Foo> foos = new List<Foo>();
            foreach (string s in obj.Split('\n'))
            {
                if (!string.IsNullOrWhiteSpace(s))
                {
                    var fooFromJson = JsonConvert.DeserializeObject<Foo>(s);
                    foos.Add(fooFromJson);
                }
            }
            return foos.Count;
        }
    }
}
